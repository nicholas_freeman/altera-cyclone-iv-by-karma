`define DIV_INIT_VAL 32'd11

module generator(
input clk,
input btn_in,
output out,
output led,
output [9:0] bar);

	wire button;
	uint32_t divisor = `DIV_INIT_VAL;
	
	always_ff @(negedge button) begin
		if(divisor != 0)
			divisor--;
		else
			divisor <= `DIV_INIT_VAL;
	end


	uint32_t timer;
	always_ff @(posedge pll_cur) timer++;
		
	myPLL pll(.inclk0(clk), .c0(pll_out), .locked(pll_rdy));	
	debounce(.in(btn_in), .clk(timer[14]), .out(button));
	clock_dev(.div(divisor), .clk(pll_cur), .out(osc));
	
	
	wire pll_out;
	wire pll_rdy;
	
	assign pll_cur = pll_out & pll_rdy;
	assign led = pll_rdy;
	assign bar[9:0] = divisor[9:0];

endmodule


module clock_dev(input [31:0] div, input clk, output reg out);

	uint32_t cnt = 0;
	always_ff @(posedge clk) begin
		if((cnt < (div - 1) / 2))
			cnt++;
		else begin
			cnt <= 0;
			out = !out;
		end
	end
	
endmodule


module debounce(input wire in, input wire clk, output wire out);

	clock_dev(.div(50_000_000/10_000), .clk(clk), .out(debounce_clk));

	uint32_t filter = 0;
	reg res = 0;
	byte i = 0;
	
	assign out = res;

	always_ff @(posedge debounce_clk) begin
		filter <<= 1;
		filter += in;
		res = filter[0];
		for(i = 1; i < 32; i++)
			res &= filter[i];
	end
	
endmodule

