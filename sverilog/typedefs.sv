`define CLK_FREQ_HZ	50_000_000
//`define CLK_FREQ_HZ	282_812_500
//`define CLK_FREQ_HZ	141_406_250
`define GEN_DEV		(`CLK_FREQ_HZ / 1_000)

parameter FIFO_ADDR_SIZE = 13;
parameter FIFO_SIZE = 4_000;


typedef reg[31:0] uint32_t;
typedef reg[15:0] uint16_t;
typedef reg[7:0]  uint8_t;
typedef reg[5:0]  uint6_t;
typedef reg[3:0]  uint4_t;

