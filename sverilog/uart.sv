`define UART_BAUDRATE		115200
`define UART_WAIT_CYCLES   16 		// ( < 64)
`define UART_FR_RDY			0
`define UART_FR_TXBUSY		1
`define UART_FR_RXBUSY		2

`define UART_CR_TXSRT		1

module uart(
input  clk,
output tx, 
input  rx, 
input  uint8_t txd,
output uint8_t rxd,
input  uint8_t cr,
output uint8_t fr,
output reg test
);

	initial begin
		fr[`UART_FR_RDY] = 1;
	end


	wire uart_clk;
	clock_dev(.div(`CLK_FREQ_HZ/(`UART_BAUDRATE * `UART_WAIT_CYCLES)), .clk(clk), .out(uart_clk));

	
	uint6_t tx_cnt;
	uint4_t tx_send_qty;

	reg [9:0] txd_buf = 1;
	assign tx = txd_buf[0];

	always @(posedge uart_clk) begin
	
		if(cr[`UART_CR_TXSRT]) begin
			if(tx_send_qty == 0) begin
				txd_buf <= (txd << 1) | (1 << 9);
				tx_send_qty++;
				
			end else if(tx_send_qty < 11) begin
			
				if(tx_cnt < (`UART_WAIT_CYCLES - 1)) begin
					tx_cnt++;
				end else begin
					tx_cnt <= 0;
					tx_send_qty++;
					txd_buf >>= 1;
				end

			end else begin
				txd_buf <= 1;
				tx_send_qty <= 0;
			end
			
		end
		//end else begin
		//	txd_buf <= 1;
		//	tx_send_qty <= 0;
		//end
	end 


	uint6_t rx_cnt;
	uint4_t rx_recv_qty;

	reg rx_start = 0;
	reg [9:0] rxd_buf;

	always @(posedge uart_clk) begin
		if(!rx & !rx_start) begin
			rx_start <= 1;
		end
		
		if(rx_start) begin
		
			if(rx_recv_qty < 10) begin
			
				if(rx_cnt == (`UART_WAIT_CYCLES) / 2) begin
					rx_recv_qty++;
					rxd_buf >>= 1;
					test = !test;
				end else begin
					rxd_buf[9] <= rx;
				end
			
				if(rx_cnt < (`UART_WAIT_CYCLES - 1)) begin
					rx_cnt++;
				end else begin
					rx_cnt <= 0;
				end
				
			end else begin
				rxd[7:0] <= rxd_buf[9:0];
				rx_cnt <= 0;
				rx_recv_qty <= 0;
				rx_start <= 0;
			end
			
		end
	end
	
assign fr[`UART_FR_RDY] = tx_send_qty < 9;
assign fr[`UART_FR_TXBUSY] = tx_send_qty > 0;
assign fr[`UART_FR_RXBUSY] = rx_recv_qty > 0;

endmodule



