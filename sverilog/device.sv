
module device(
(* chip_pin = "E16" *) input CLK,
(* chip_pin = "B16" *) input BTN,
(* chip_pin = "L2" *)  output OSC_1,
(* chip_pin = "T2" *)  output OSC_2,
(* chip_pin = "D12" *) output led_1,
(* chip_pin = "C11" *) output led_2,
(* chip_pin = "B10" *) output led_3,
(* chip_pin = "B7" *)  output led_4,
(* chip_pin = "L6" *)  input  RX,
(* chip_pin = "P8" *)  output TX,
(* chip_pin = "R12, R13, R14, R16, P16, N16, L16, K16, J16, G15" *) output [9:0] bar);


	wire button;
	wire button_inv = !button;
	debounce deb1(.in(BTN), .clk(CLK), .out(button));


	wire pll_out_clk;
	myPLL pll(.inclk0(CLK), .c0(pll_out_clk));


	clock_dev(.div(5_000_000), .clk(CLK), .out(OSC_1));
	//assign OSC_1 = BTN;
	//assign OSC_2 = button;
	
	//assign bar[9:0] = timer[9:0];
	//assign OSC_2 = !OSC_1;



	wire [7:0] uart_cr;
	wire [7:0] uart_fr;
	wire [7:0] rx_byte;
	wire [7:0] tx_byte;
	assign bar = rx_byte;
	
	wire fifo_error;
	wire fifo_empty;

	assign uart_cr[1] = (!button & !fifo_empty);
	
	uart(.clk(CLK), .tx(TX), .rx(RX), .txd(tx_byte), .rxd(rx_byte), .cr(uart_cr), .fr(uart_fr), .test(OSC_2));
	fifo(.clk(CLK), .in(rx_byte), .out(tx_byte), .trig_in(uart_fr[2]), .trig_out(uart_fr[1]), .empty(fifo_empty), .error(fifo_error));
	
	assign led_1 = OSC_1;
	assign led_2 = !uart_fr[1];
	assign led_3 = !uart_fr[2];
	assign led_4 = !fifo_error;
endmodule



module fifo(
input clk, 
input uint8_t in, 
output uint8_t out, 
input trig_in, 
input trig_out, 
output reg empty, 
output reg error);

	reg [FIFO_ADDR_SIZE:0] in_ind = 0;
	reg [FIFO_ADDR_SIZE:0] out_ind = 0;
	
	assign empty = (in_ind == out_ind);
	assign error = (in_ind == (out_ind - 1));

	(* ramstyle = "M144K" *) uint8_t data_arr[0:FIFO_SIZE];

	always_ff @(negedge trig_in) begin
		//if(in_ind < FIFO_SIZE) begin
			if(in_ind != (out_ind - 1)) begin
				in_ind++;
				data_arr[in_ind] <= in;
			end
		//end else begin
		//	in_ind <= 0;
		//end
	end

	always_ff @(negedge trig_out) begin
		//if(out_ind < FIFO_SIZE) begin
			if(out_ind != in_ind) begin
				out_ind++;
				out <= data_arr[out_ind];
			end
		//end else begin
		//	out_ind <= 0;
		//end
	end
	

endmodule


