derive_clock_uncertainty
create_clock -add -period 100MHz -name {CLK} [get_ports {CLK}]
create_clock -add -period 400MHz -name {OSC} [get_ports *]
create_clock -add -period 400MHz -name {pll_cur} [get_ports *]
